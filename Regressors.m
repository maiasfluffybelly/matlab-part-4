classdef Regressors
    properties
        threeDimArrayOfGrasps
        arrayOfLabels
        averageAccuracy 
        numberOfKfolds
        numberOfFeatures
        RFFeatureRanks
    end
    methods
        function obj = Regressors(threeDimArrayOfGrasps, arrayOfLabels,numberOfKfolds, numberOfFeatures)
            obj.threeDimArrayOfGrasps = threeDimArrayOfGrasps;
            obj.arrayOfLabels = arrayOfLabels;
            obj.numberOfKfolds = numberOfKfolds;
            obj.numberOfFeatures = numberOfFeatures;
            obj.averageAccuracy = 0;  
            obj.RFFeatureRanks = zeros(numberOfFeatures,6);
        end
        
        function foldingIndicies = produceCrossValidationFoldIndex(obj)% folds the data into 'k folds'
            foldWidth = floor(size(obj.threeDimArrayOfGrasps,1)/obj.numberOfKfolds);
            foldingIndicies = zeros(2,obj.numberOfKfolds);
            for fold = 0:(obj.numberOfKfolds - 1)
                foldingIndicies(1,(fold+1)) = foldWidth*fold+1; 
                foldingIndicies(2,(fold+1)) = foldWidth*(fold + 1);
            end
        end
        
        
        function LROutput = LinearRegressor(obj,trainingGroup, testGroup)
            LROutput = double.empty(size(trainingGroup,1),0);
            %fittedCoefficentsTotal = double.empty((obj.numberOfFeatures+6),0);
            singleLRFit = zeros(size(testGroup,1),1);
            for responseVariables = 1:6
                linearFit = fitglm(trainingGroup(:,7:(6+obj.numberOfFeatures)),trainingGroup(:,responseVariables),'linear');
                %fittedResponse = linearFit.Fitted.Response;
                fittedCoefficients = linearFit.Coefficients.Estimate;
                singleLRFit = singleLRFit + fittedCoefficients(1,1)*ones(size(testGroup,1),1);
                for EMGIterator = 1:obj.numberOfFeatures
                    singleLRFit = singleLRFit + fittedCoefficients((EMGIterator+1),1).*testGroup(:,(6+EMGIterator));
                end
                %fittedCoefficentsTotal = [fittedCoefficentsTotal,fittedCoefficients];
                LROutput = [LROutput, singleLRFit];
                singleLRFit = zeros(size(testGroup,1),1);
            end
            
        end
        
        function SVROutput = SupportVectorRegressor(obj,trainingGroup, testGroup)
            SVROutput = double.empty(size(trainingGroup,1),0);
            singleSVRFit = zeros(size(testGroup,1),1);
            for responseVariables = 1:6
                SVRmodel = fitrsvm(trainingGroup(:,7:(6+obj.numberOfFeatures)),trainingGroup(:,responseVariables),'KernelFunction','linear');
                singleSVRFit = predict(SVRmodel,testGroup(:,7:(6+obj.numberOfFeatures)));
                SVROutput = [SVROutput, singleSVRFit];
                    
            end

        end
        
        function NNOutput = NeuralNetworkRegressor(obj,trainingGroup, testGroup,numberOfHiddenNeurons)
            net = feedforwardnet(numberOfHiddenNeurons); % according to this page the number of neurons should be about the average of number of inputs (8) and outputs (1)
            %https://stats.stackexchange.com/questions/181/how-to-choose-the-number-of-hidden-layers-and-nodes-in-a-feedforward-neural-netw
            NNLabels = transpose(trainingGroup(:,1:6));
            net = train(net,transpose(trainingGroup(:,7:(obj.numberOfFeatures+6))),NNLabels);
            NNOutput = net(transpose(testGroup(:,7:(obj.numberOfFeatures+6))));
            
        end
        
        function [RFOutput,obj] = RandomForestRegressor(obj,trainingGroup, testGroup, numberOfTrees)
            RFOutput = double.empty(size(trainingGroup,1),0);
            singleRFFit = zeros(size(testGroup,1),1);
            for responseVariables = 1:6
                RFmodel = fitrensemble(trainingGroup(:,7:(6+obj.numberOfFeatures)),trainingGroup(:,responseVariables),'NumLearningCycles',numberOfTrees);
                singleRFFit = predict(RFmodel,testGroup(:,7:(6+obj.numberOfFeatures)));
                RFOutput = [RFOutput, singleRFFit];
                
                [idx,RFFeatureRank] = fsrftest(testGroup(:,7:(6+obj.numberOfFeatures)),singleRFFit); % find random forest feature ranks
                            
                for iterator = 1:length(RFFeatureRank)
                    obj.RFFeatureRanks(iterator,responseVariables) =  obj.RFFeatureRanks(iterator,responseVariables) + RFFeatureRank(iterator)/size(RFFeatureRank,1);
                end
            end
            
            

        end
        
        function featureRanksOut = returnFeatureRanks(obj)% returns the feature ranks array (average of all folds)
            featureRanksOut = obj.RFFeatureRanks;
        end
    end
    
end

