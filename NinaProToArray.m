
classdef NinaProToArray
    
    properties
        subjectIndex %current subject (data corresponds to)
        graspIndex %current grasp
        subjects %array of subject names
        grasps %array of grasp names
        trials %number of trials
        fileName %used for creating csv
        twoDimensionalArray %current EMG data
        listPairArray %indexes of current grasp type sections of data
        graspArray %array of grasp sequence in data
        exerciseIndex %current exercise set
        columns %number of EMG channels (columns of data)
        exercises %list of exercise sets in data
        graspNumber %number of grasps in exercise
        graspList %records current grasp for each index pair
        graspIndexCount %index for graspList
    end
    
    properties (Dependent)
        RearrangedFile
    end
    methods
        
        %Collects current loop iteration data 
        function self = NinaProToArray(subjectIndex,exerciseIndex,subjects,exercises,columns, graspNumber)
            self.subjectIndex = subjectIndex;
            self.exerciseIndex = exerciseIndex;
            self.subjects = subjects;
            self.exercises = exercises;
            self.columns = columns;
            self.graspNumber = graspNumber;
            self.twoDimensionalArray = zeros(10,9);
        end
         
        %Collects the EMG and stimulus data for current iteration/dataset
        function self = arraySort(self) 
            disp(self.twoDimensionalArray)
            filename = (strcat(self.subjects(self.subjectIndex),{'_A1_'},string(self.exercises(self.exerciseIndex)),{'.mat'}));
            disp(filename);
            load(filename,'emg','restimulus') %restimulus had has some pre-processing
            self.twoDimensionalArray = emg; 
            self.twoDimensionalArray(:,(self.columns+1)) = restimulus;
        end

        %Find row indexes of grasp sections to seperate into grasp files
        function self = organiseByTriggers(self)
            %3D Array to collect data from csv files and concatanate them. 
            %Takes 10 by however many rows,
            %selects indexers accroding to triggers...(3rd dimension)
            %new start = current+1 row, clomun 1. 
            self.listPairArray = int64.empty(0,0);
            startRow = 1;
            currentGrasp = 0;
            self.graspIndexCount = 1;
            A = self.twoDimensionalArray;
            if isempty(A) == false
                for indexRow = 1:length(A)
                    triggerState = A(indexRow,self.columns+1);
                    if triggerState ~= currentGrasp
                        endRow = indexRow-1;
                        currentGrasp = triggerState;
                        self.listPairArray = ...
                            [self.listPairArray ; [startRow,endRow]]; 
                        self.graspList(self.graspIndexCount) = triggerState;
                        self.graspIndexCount = self.graspIndexCount +1;
                        startRow = indexRow;
                    end

                end 
            end
        end
        
        %Creates the new csv files for each grasp and exercise set
        function self = RearrangeNewData(self, rearrangeBy)
            if rearrangeBy == 1% 1 is selected to rearrange the files by grasp types only potential for other rearranging options
                self.graspArray = int64.empty(0,8);
                disp(length(self.listPairArray));
                
                %For each new grasp section create a new csv dataset
                for listPairIndex = 1:length(self.listPairArray)
                    self.graspArray = [self.twoDimensionalArray((self.listPairArray(listPairIndex,1)):(self.listPairArray(listPairIndex,2)),1:self.columns)];
                    graspFileName = char(strcat('E',num2str(self.exerciseIndex),'_grasp' ,num2str(self.graspList(listPairIndex)), '.csv'));

                    if isfile(graspFileName)% if the grasp file already exists-append, if it does not make a new file
                        dlmwrite(graspFileName,self.graspArray,'delimiter',',','-append');
                    else
                        dlmwrite(graspFileName,self.graspArray,'delimiter',',');
                    end
                end
                
                %Clears for next dataset
                self.graspArray = [];
                self.listPairArray = [];
            end
        end
    end
    
end



   



     