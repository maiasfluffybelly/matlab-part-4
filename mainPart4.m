clear;
close all;
selectStagesToRun = [true,true,true,true]; %boolean array the size of the total number of stages in the program, decides which stages to run
% ! make sure to generate grasp files before doing anything else
% stage 2 should run if you want to run any other later stages as it
% generates the 3D array processed in later functions
arrayOfSubjects = {'AD','LY'};
arrayOfGrasps = {'CC','PI','PO'};
numberOfTrials = 3; %max number of trials for subjects

% parameters for NinaPro dataset
arrayOfSubjectsNinaPro = {'S1','S2','S3'};
arrayOfExercisesNinaPro = {'E1','E2','E3'};
arrayOfGraspsPerExerciseNinaPro = [12,17,23];


%% control options for first stage (experimental test CSV -> labelled CSV for classifier) ! run this once or else it will keep adding to existing files
inputDelay = 6; %delay during grasp recordings, cuts out dynamic part of grasp in hundreds of milliseconds
rearrangeBy = 1; % if 1 is selected the file rearranger will rearrange the files into large arrays of grasps only
equalSize = true; % make sure that each rest and grasp test is the same number of rows
isNinaPro = true; %does the data come from the NinaPro datasets, this will process the files differently, it also alters how the files are concatenated into the 3D array (second stage)

%% control options for second stage (labelled CSV -> 3D organised array of different groups) 
sequenceType = [0,1]; % rest = 0 and grasp = 1
restSelectedInSequence = 1; % pick which rest from rest grasp set to be used in the classifier code
desiredNumberOfTrials = 6000;% good for ninapro with 1 subject%194187 good for first dataset;%324966; % how many rows or data points we want to feed the classifiers should be as big as the smallest number of rows of a grasp file
%desiredNumberOfTrials = 2000;% good for ninapro with 1 subject%194187 good for first dataset;%324966; % how many rows or data points we want to feed the classifiers should be as big as the smallest number of rows of a grasp file
numberOfFeatures = 10; % number of EMG channels which to the classifiers is the number of features has to be the same or less than the largest number of features in the file
numberOfClasses = -1; % optional lower number of classes, otherwise set this to -1

%% control options for third stage filters are applied at this stage
order = 4; %For Savitsky Golay filtering
framelength = 200; %For Savitsky Golay filtering filtering-must be odd number
movingAverageLength = 200;
whichFilter = 3; % 1 = moving average, 2 = savitsky golay filter
stepSize = 1; %Anany's Filter
windowSlide = 5; %Anany's Filter

%% control options for fourth stage (apply classifiers and output confusion matrix)
kFoldsForCrossValidation = 5; %currently set to 5, 5 fold cross validation
SelectClassifiersToRun = [false,false,true,false,false]; % select which classifiers to run: [LDA,KNN,RF,NN,SVM]

% hyperparameter selection for classifiers:
numberOfTrees = 5; % number of trees hyperparameter for random forest classification, works well rn with 5 but apparently RF cannot be overfit so can have many trees (although takes ages)
%https://stats.stackexchange.com/questions/36165/does-the-optimal-number-of-trees-in-a-random-forest-depend-on-the-number-of-pred
numberOfNeighbours = 221; % K value is N^(1/2)/2 N = Number of training Samples, odd value suggested online:
%https://stackoverflow.com/questions/11568897/value-of-k-in-k-nearest-neighbor-algorithm
numberOfHiddenNeurons = 8; % according to this page the number of neurons should be a number ranging from the number of inputs (8) and outputs (1)
%https://stats.stackexchange.com/questions/181/how-to-choose-the-number-of-hidden-layers-and-nodes-in-a-feedforward-neural-netw

if selectStagesToRun(1) == true % reads from the csv/ mat files and converts them into grasp related files
    if ~isNinaPro %is this NinaPro data?
        for subjectIndex = 1:length(arrayOfSubjects)
            for graspIndex = 1:length(arrayOfGrasps)
                for trialIndex = 1:numberOfTrials
                    dataset = FileToDataset(subjectIndex,graspIndex,arrayOfSubjects,arrayOfGrasps,trialIndex,8); %create instance of class FileToDataset
                    dataset = dataset.csvToArray();
                    dataset = dataset.organiseByTrigger(inputDelay,equalSize);
                    dataset = dataset.RearrangeData(rearrangeBy);
                end
            end
        end
    else 
       for subjectIndex = 1:length(arrayOfSubjectsNinaPro)
            for exerciseIndex = 1:length(arrayOfExercisesNinaPro)
                    newdataset = NinaProToArray(subjectIndex,exerciseIndex,arrayOfSubjectsNinaPro,arrayOfExercisesNinaPro,10,sum(arrayOfGraspsPerExerciseNinaPro)); %create instance of class FileToDataset
                    newdataset = newdataset.arraySort();
                    newdataset = newdataset.organiseByTriggers();
                    newdataset = newdataset.RearrangeNewData(1);
            end
       end
    end
end



arrayOfGraspTypes = double.empty(desiredNumberOfTrials,numberOfFeatures,0); %unfolded grasp type array
graspLabelForLayer = {};

if selectStagesToRun(2) == true % converts to a 3D array of grasps
    % this will force the length of the grasp arrays to be equal so that the classifer is not biased towards a class
    
    if isNinaPro == true % set the file inputs for the methods in the DatasetToArray class to be compatable with NinaPro data
    sequenceTypeIndex = 0;
    arrayOfGrasps = arrayOfExercisesNinaPro;
    arrayOfSubjects = arrayOfSubjectsNinaPro;
    
        for graspIndex = 1:length(arrayOfGraspsPerExerciseNinaPro)
            for graspPerExerciseIterate = 1:arrayOfGraspsPerExerciseNinaPro(graspIndex)+1
                if (graspPerExerciseIterate > 1 || graspIndex == restSelectedInSequence)% removes the 'rests' that are not the one selected by user
                    sequenceTypeIndex = sequenceTypeIndex + 1;
                    sequenceType(sequenceTypeIndex) = graspPerExerciseIterate-1;
                end
            end

        end
    
    end

    %initialising variables used in this loop
    iteration = 0;
    vectorV = ones(1,movingAverageLength)/movingAverageLength; %moving average vector for convolution with data.
    
    counter = 0;
    for graspIndex = 1:length(arrayOfGrasps)
        if isNinaPro % the sequence is in a set of grasps per exercise for NinaPro and is in a pair of rest, grasp for not NinaPro
            sequenceLength = arrayOfGraspsPerExerciseNinaPro(graspIndex)+1;
        else
            sequenceLength = length(sequenceType);
        end
        
        for sequenceTypeIndex = 1:sequenceLength % makes sure we only add one file of 'rest type'
            if ((iteration < numberOfClasses) || (numberOfClasses == -1)) && (sequenceTypeIndex > 1 || graspIndex == restSelectedInSequence) % limits number of classes if desired
                iteration = iteration + 1;
                arrayData = DatasetToArray(graspIndex,arrayOfGrasps,sequenceType,sequenceTypeIndex,isNinaPro); %create instance of class FileToDataset
                arrayOfGraspTypes = arrayData.GraspCsvToArray(iteration,desiredNumberOfTrials,arrayOfGraspTypes, numberOfFeatures);
                graspLabelForLayer = arrayData.applyArrayLabels(graspLabelForLayer, iteration);
            end
        end    
        
    end
end

if selectStagesToRun(3) == true % apply a filter and make the EMG readings into magnitude rather than magnitude and sign
    szdim3 = size(arrayOfGraspTypes,3); 
    
    if whichFilter == 1
        %Moving Average Filter attempt
        for iterationFilter = 1:szdim3
             filteredData = Filters(order, framelength, iterationFilter, numberOfFeatures, vectorV, movingAverageLength,stepSize,windowSlide);
             arrayOfGraspTypesNew(:,:,iterationFilter) = filteredData.movingAverage(abs(arrayOfGraspTypes(:,:,iterationFilter)), movingAverageLength);
        end
    
    elseif whichFilter == 2
        %Applies SG filter
        %test = abs(arrayData.outputArray());
        for iterationFilter = 1:szdim3
            filteredData = Filters( order, framelength, iterationFilter, numberOfFeatures, vectorV, movingAverageLength,stepSize,windowSlide);
            arrayOfGraspTypesNew(:,:,iterationFilter) = filteredData.SavitzkyGFilter(abs(arrayOfGraspTypes(:,:,iterationFilter)));
        end
        
    elseif whichFilter == 3
        for iterationFilter = 1:szdim3
            filteredData = Filters( order, framelength, iterationFilter, numberOfFeatures, vectorV, movingAverageLength,stepSize,windowSlide);
           % timeDomainFeatures = TimeDomainFE_Ver3(arrayOfGraspTypes(:,:,iterationFilter),1,1,200,10,1,8);
            arrayOfGraspTypesNew(:,:,iterationFilter) = filteredData.TimeDomainFE_Ver3((arrayOfGraspTypes(:,:,iterationFilter)));
        end
    end
        
end



if selectStagesToRun(4) == true % apply selected classifiers and output their performance
    % create classifier instance
    classifierInstanceLDA = Classifiers(arrayOfGraspTypesNew, graspLabelForLayer,kFoldsForCrossValidation, numberOfFeatures);
    classifierInstanceKNN = Classifiers(arrayOfGraspTypesNew, graspLabelForLayer,kFoldsForCrossValidation, numberOfFeatures);
    classifierInstanceRF = Classifiers(arrayOfGraspTypesNew, graspLabelForLayer,kFoldsForCrossValidation, numberOfFeatures);
    classifierInstanceNN = Classifiers(arrayOfGraspTypesNew, graspLabelForLayer,kFoldsForCrossValidation, numberOfFeatures);
    classifierInstanceSVM = Classifiers(arrayOfGraspTypesNew, graspLabelForLayer,kFoldsForCrossValidation, numberOfFeatures);
    
    foldIndexOfGrasps = classifierInstanceLDA.produceCrossValidationFoldIndex();

    % one fold is selected as the testing fold and the remainder folds are
    % selected as the training folds
    trainingGroup = double.empty(0,(numberOfFeatures+1));

    for testFoldSelected = 1:kFoldsForCrossValidation
        testGroup = classifierInstanceLDA.Convert3DarrayInto2DLabelledArray(arrayOfGraspTypesNew(foldIndexOfGrasps(1,testFoldSelected):foldIndexOfGrasps(2,testFoldSelected),:,1:length(graspLabelForLayer)));
        
        for trainingFoldIterator = 1:kFoldsForCrossValidation
            if(trainingFoldIterator ~= testFoldSelected)
                trainingGroup = [trainingGroup;classifierInstanceLDA.Convert3DarrayInto2DLabelledArray(arrayOfGraspTypesNew(foldIndexOfGrasps(1,trainingFoldIterator):foldIndexOfGrasps(2,trainingFoldIterator),:,1:length(graspLabelForLayer)))];
            end
        end
        
        trainingFoldCount = 0;
        %apply classifiers here
        if SelectClassifiersToRun(1) == 1 % Runs LDA classifier and outputs a confusion matrix
            ldaOutput = classifierInstanceLDA.LinearDiscriminantAnalyser(trainingGroup, testGroup);
            ldaMatrix = classifierInstanceLDA.generateConfusionMatrix(testGroup,ldaOutput)
            classifierInstanceLDA = classifierInstanceLDA.generateAverageAccuracy(ldaMatrix);
        end
        
        if SelectClassifiersToRun(2) == 1 %Runs KNN classifier and outputs a confusion matrix
            KNNOutput = classifierInstanceKNN.KNearestNeighboursClassifier(trainingGroup, testGroup, numberOfNeighbours);
            KNNMatrix = classifierInstanceKNN.generateConfusionMatrix(testGroup,KNNOutput)
            classifierInstanceKNN = classifierInstanceKNN.generateAverageAccuracy(KNNMatrix);
        end
        
        if SelectClassifiersToRun(3) == 1 %Runs RF classifier and outputs a confusion matrix (quite slow with only 5 trees, a few mins if you want 50 trees (I got about 0.9838 accuracy with 50) although pretty good with 5 trees)
            [RFOutput,classifierInstanceRF] = classifierInstanceRF.RandomForestClassifier(trainingGroup, testGroup, numberOfTrees);
            RFMatrix = classifierInstanceRF.generateConfusionMatrix(testGroup,str2double(RFOutput))
            classifierInstanceRF = classifierInstanceRF.generateAverageAccuracy(RFMatrix);
        end
        
        if SelectClassifiersToRun(4) == 1 %Runs NN classifier and outputs a confusion matrix (even slower to output, you have to wait a few mins has a little window to show it's progress when running)
            NNOutput = classifierInstanceNN.NeuralNetworkClassifier(trainingGroup,testGroup,numberOfHiddenNeurons);
            NNMatrix = classifierInstanceNN.generateConfusionMatrix(testGroup,NNOutput)
            classifierInstanceNN = classifierInstanceNN.generateAverageAccuracy(NNMatrix);
        end
        
        if SelectClassifiersToRun(5) == 1%Runs SVM classifier and outputs a confusion matrix
            SVMOutput = classifierInstanceSVM.SupportVectorMachineClassifier(trainingGroup, testGroup);
            SVMMatrix = classifierInstanceSVM.generateConfusionMatrix(testGroup,SVMOutput)
            classifierInstanceSVM = classifierInstanceSVM.generateAverageAccuracy(SVMMatrix);
        end
        trainingGroup = double.empty(0,(numberOfFeatures+1));

    end
    
    ldaAverageAccuracy = classifierInstanceLDA.returnAverageAccuracy();
    KNNAverageAccuracy = classifierInstanceKNN.returnAverageAccuracy();
    RFAverageAccuracy = classifierInstanceRF.returnAverageAccuracy();
    RFFeatureRanksTotal = classifierInstanceRF.returnFeatureRanks();
    NNAverageAccuracy = classifierInstanceNN.returnAverageAccuracy();
    SVMAverageAccuracy = classifierInstanceSVM.returnAverageAccuracy();
    
    disp(ldaAverageAccuracy);
    disp(KNNAverageAccuracy);
    disp(RFAverageAccuracy);
    disp(NNAverageAccuracy);
    disp(SVMAverageAccuracy);
end