classdef FileToDataset
    
    properties
        subjectIndex
        graspIndex
        subjects %array of subject names
        grasps %array of grasp names
        trials %number of trials
        fileName
        twoDimensionalArray
        listPairArray
        graspArray
        restArray
        EMGColumns
    end
    
    properties (Dependent)
        RearrangedFile
    end
    methods
        
        function self = FileToDataset(subjectIndex,graspIndex, subjects, grasps, trials, EMGColumns)
            self.subjectIndex = subjectIndex;
            self.graspIndex = graspIndex;
            self.subjects = subjects;
            self.grasps = grasps;
            self.trials = trials;
            self.EMGColumns = EMGColumns;
            self.twoDimensionalArray = zeros(10000,EMGColumns+1);
        end
         
        function self = csvToArray(self) 
            path = strcat('Experiments\',self.subjects(self.subjectIndex));
            self.fileName = strcat(self.subjects(self.subjectIndex),{'_Trial'},string(self.trials),{'_'},self.grasps(self.graspIndex));
            disp(self.fileName)
            if isfile(strcat(path,'\',self.fileName,'.csv'))
                self.twoDimensionalArray = [];
                self.twoDimensionalArray = csvread(strcat(path,'\',self.fileName,'.csv'),2); 
            else 
                self.twoDimensionalArray = [];
            end
            
        end
        
        function self = organiseByTrigger(self,delay,equalSize)
            %3D Array to collect data from csv files and concatanate them. 
            %Takes 9 (currently, can be changed with the function input) by however many rows,
            %selects indexers accroding to triggers...(3rd dimension)
            %new start = current+1 row, clomun 1. 
            self.listPairArray = int64.empty(0,0);
            startRow = 1;
            triggerStateCount = 0;
            A = self.twoDimensionalArray;
            if isempty(A) == false
                for indexRow = 1:length(A)
                    triggerState = A(indexRow,(self.EMGColumns+1));
                    if triggerState == 1
                        triggerStateCount = triggerStateCount + 1;
                        
                        if (equalSize == true) % if you want everything to be of equal size
                            newStartRow = indexRow+1+delay*100; % rest array is also shortened to match and to remove the starting part of rest array
                            self.listPairArray = [self.listPairArray ; [startRow+1,(startRow+1921-delay*100)]]; % subtract delay portion is so that it does not go over into the section for the other phase
                        else
                            if mod(triggerStateCount,2) == 0 % if it is even aka the tests are done rest, grasp, rest
                                newStartRow = indexRow+1;
                                endRow = indexRow - delay*100;
                            else
                                newStartRow = indexRow+1+delay*100;
                                endRow = indexRow; 
                            end
                            self.listPairArray = [self.listPairArray ; [startRow+1,endRow+1]];
                        end
                        
                        startRow = newStartRow; 
                    end

                end 
            end
        end
        
        function self = RearrangeData(self, rearrangeBy)
            if rearrangeBy == 1% 1 is selected to rearrange the files by grasp types only potential for other rearranging options
                self.graspArray = int64.empty(0,self.EMGColumns);
                self.restArray = int64.empty(0,self.EMGColumns);
                disp(length(self.listPairArray));
                for listPairIndex = 2:(length(self.listPairArray)-2) %using the list pair index information the alternating rest and grasp phases are taken out of twoDimensionalArray
                    if mod(listPairIndex,2) == 0 
                        self.graspArray = [self.graspArray; self.twoDimensionalArray((self.listPairArray(listPairIndex,1)):(self.listPairArray(listPairIndex,2)),1:self.EMGColumns)];
                    else
                        self.restArray = [self.restArray; self.twoDimensionalArray((self.listPairArray(listPairIndex,1)):(self.listPairArray(listPairIndex,2)),1:self.EMGColumns)];
                    end
                end
                
                graspFileName = char(strcat(cellstr(self.grasps(self.graspIndex)),'_grasp.csv'));
                restFileName = char(strcat(cellstr(self.grasps(self.graspIndex)),'_rest.csv'));
                
                if isfile(graspFileName)% if the grasp file already exists-append, if it does not make a new file
                    dlmwrite(graspFileName,self.graspArray,'delimiter',',','-append');
                    dlmwrite(restFileName,self.restArray,'delimiter',',','-append');
                else
                    dlmwrite(graspFileName,self.graspArray,'delimiter',',');
                    dlmwrite(restFileName,self.restArray,'delimiter',',');
                end
                
                self.graspArray = [];
                self.restArray = [];
            end
        end
    end
    
end

