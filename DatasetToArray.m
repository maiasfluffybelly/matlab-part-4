classdef DatasetToArray
    
    properties
        TwoDArray %output 2D array for current input csv
        graspIndex %current grasp if looping 
        grasps %array of grasp names
        sequenceType
        sequenceTypeIndex
        fileNameSequence
    end

    methods
        
        function obj = DatasetToArray(graspIndex, grasps, sequenceType, sequenceTypeIndex)
            obj.graspIndex = graspIndex;
            obj.grasps = grasps;
            obj.sequenceType = sequenceType;
            obj.sequenceTypeIndex = sequenceTypeIndex;
        end
        
        function ThreeDArrayOut= GraspCsvToArrayDefault(obj, iteration, desiredNumberOfTrials, ThreeDArrayIn, numberOfFeatures) % outputs a 3D array of grasps from the grasp file inputs
            if obj.sequenceType(obj.sequenceTypeIndex) == 0
                   obj.fileNameSequence = strcat(string(obj.grasps(obj.graspIndex)),{'_rest.csv'}); 
            else
                   obj.fileNameSequence = strcat(string(obj.grasps(obj.graspIndex)),{'_grasp.csv'});
            end
            
            disp(obj.fileNameSequence);
            obj.TwoDArray = csvread(obj.fileNameSequence); %Converts csv files to 2D arrays 
            ThreeDArrayOut(1:desiredNumberOfTrials,:,1:(iteration-1)) = ThreeDArrayIn(1:desiredNumberOfTrials,:,:);
            ThreeDArrayOut(1:desiredNumberOfTrials,:,iteration) = obj.TwoDArray(1:desiredNumberOfTrials,1:numberOfFeatures); %Stores latest 2D array conversion to a 3D array output
        end
        
        function ThreeDArrayOut= GraspCsvToArrayNinaPro(obj, iteration, desiredNumberOfTrials, ThreeDArrayIn, numberOfFeatures) % outputs a 3D array of grasps from the grasp file inputs       
            obj.fileNameSequence = strcat(string(obj.grasps(obj.graspIndex)),{'_grasp'},string(obj.sequenceType(iteration)),{'.csv'});
            disp(obj.fileNameSequence);
            obj.TwoDArray = csvread(obj.fileNameSequence); %Converts csv files to 2D arrays 
            ThreeDArrayOut(1:desiredNumberOfTrials,:,1:(iteration-1)) = ThreeDArrayIn(1:desiredNumberOfTrials,:,:);
            ThreeDArrayOut(1:desiredNumberOfTrials,:,iteration) = obj.TwoDArray(1:desiredNumberOfTrials,1:numberOfFeatures); %Stores latest 2D array conversion to a 3D array output
        end
        
        function labelArrayOut = applyArrayLabelsDefault(obj, labelArrayIn)% gives a label for all of different classes useful in determining which grasps have what accuracy
           graspName = strcat(obj.grasps(obj.graspIndex),{'_'},string(obj.sequenceType(obj.sequenceTypeIndex)));
           labelArrayOut = [labelArrayIn,graspName];
        end
        
        function labelArrayOut = applyArrayLabelsNinaPro(obj, labelArrayIn, iteration)% gives a label for all of different classes useful in determining which grasps have what accuracy
          graspName = strcat(obj.grasps(obj.graspIndex),{'_'},string(obj.sequenceType(iteration)));
          labelArrayOut = [labelArrayIn,graspName];
        end
    end
end

  