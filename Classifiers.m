classdef Classifiers% anything to do directly with classifiers goes here
    
    properties
        threeDimArrayOfGrasps
        arrayOfLabels
        averageAccuracy 
        numberOfKfolds
        numberOfFeatures
        RFFeatureRanks
    end
    
    methods
        
        function obj = Classifiers(threeDimArrayOfGrasps, arrayOfLabels,numberOfKfolds, numberOfFeatures)
            obj.threeDimArrayOfGrasps = threeDimArrayOfGrasps;
            obj.arrayOfLabels = arrayOfLabels;
            obj.numberOfKfolds = numberOfKfolds;
            obj.numberOfFeatures = numberOfFeatures;
            obj.averageAccuracy = 0;
            obj.RFFeatureRanks = zeros(1,numberOfFeatures);
        end
        
        function foldingIndicies = produceCrossValidationFoldIndex(obj)% folds the data into 'k folds'
            foldWidth = floor(size(obj.threeDimArrayOfGrasps,1)/obj.numberOfKfolds);
            foldingIndicies = zeros(2,obj.numberOfKfolds);
            for fold = 0:(obj.numberOfKfolds - 1)
                foldingIndicies(1,(fold+1)) = foldWidth*fold+1; 
                foldingIndicies(2,(fold+1)) = foldWidth*(fold + 1);
            end
        end
        
        function twoDArrayOutput = Convert3DarrayInto2DLabelledArray(obj, threeDArrayInput)% for the MATLAB classifiers to work
            for layerIterator = 1:size(threeDArrayInput,3)
                twoDArrayOutput((size(threeDArrayInput,1)*(layerIterator - 1)+1):size(threeDArrayInput,1)*(layerIterator),1:obj.numberOfFeatures) = threeDArrayInput(:,:,layerIterator);
                twoDArrayOutput((size(threeDArrayInput,1)*(layerIterator - 1)+1):size(threeDArrayInput,1)*(layerIterator),(obj.numberOfFeatures+1)) = layerIterator;
            end
        end
        
        function LDAOutput = LinearDiscriminantAnalyser(obj, trainingSet, testingSet)% uses the built in matlab LDA analyser function
            LDALabels = trainingSet(:,(obj.numberOfFeatures+1));
            ldafit = fitcdiscr(trainingSet(:,1:obj.numberOfFeatures),LDALabels);
            LDAOutput = predict(ldafit,testingSet(:,1:obj.numberOfFeatures));
        end
        
        function KNNOutput = KNearestNeighboursClassifier(obj,trainingSet, testingSet, NumberOfNeighbours)% uses the built in KNN function, added hyperaparameter selection
            KNNLabels = trainingSet(:,(obj.numberOfFeatures+1));
            KNNfit = fitcknn(trainingSet(:,1:obj.numberOfFeatures),KNNLabels,'NumNeighbors',NumberOfNeighbours,'Standardize',0);
            KNNOutput = predict(KNNfit,testingSet(:,1:obj.numberOfFeatures));
        end
        
        function SVMOutput = SupportVectorMachineClassifier(obj, trainingSet, testingSet) % uses the built in SVM function 
            SVMLabels = trainingSet(:,(obj.numberOfFeatures+1));
            SVMfit = fitcecoc(transpose(trainingSet(:,1:obj.numberOfFeatures)),SVMLabels,'Learners', 'Linear', 'Coding', 'onevsall','ObservationsIn','columns');
            SVMOutput = predict(SVMfit,testingSet(:,1:obj.numberOfFeatures));
        end
        
        
        function [RFOutput,obj] = RandomForestClassifier(obj, trainingSet, testingSet, numberOfTrees)% it needs two outputs so that it can store the averaged values of feature priorities
            RFLabels = trainingSet(:,(obj.numberOfFeatures+1));
            RFfit = TreeBagger(numberOfTrees,trainingSet(:,1:obj.numberOfFeatures),RFLabels,'OOBPrediction','On','Method','classification','OOBPredictorImportance','on');
            RFOutput = predict(RFfit,testingSet(:,1:obj.numberOfFeatures));
            
            RFFeatureRank = RFfit.OOBPermutedPredictorDeltaError; %plots feature rankings per EMG channel for each fold
            
            for i = 1:length(RFFeatureRank)
                obj.RFFeatureRanks(i) =  obj.RFFeatureRanks(i) + RFFeatureRank(i)/length(RFFeatureRank);
            end
        end
        
        function NNOutput = NeuralNetworkClassifier(obj, trainingSet, testingSet, numberOfHiddenNeurons) %uses a shallow neural network (1 hidden layer) that is feedforward
            net = feedforwardnet(numberOfHiddenNeurons); % according to this page the number of neurons should be about the average of number of inputs (8) and outputs (1)
            %https://stats.stackexchange.com/questions/181/how-to-choose-the-number-of-hidden-layers-and-nodes-in-a-feedforward-neural-netw
            NNLabels = transpose(trainingSet(:,(obj.numberOfFeatures+1)));
            net = train(net,transpose(trainingSet(:,1:obj.numberOfFeatures)),NNLabels);
            NNOutput = net(transpose(testingSet(:,1:obj.numberOfFeatures)));
            
            NNOutput = round(transpose(NNOutput));%rounds the values to classify them (so not regression but classification)
            
            for i = 1: length(NNOutput) % keeps the range of classified values between 1 and the number of classes
                if NNOutput(i) < 1
                    NNOutput(i) = 1;
                end
                if NNOutput(i) > length(obj.arrayOfLabels)
                    NNOutput(i) = length(obj.arrayOfLabels);
                end
            end
        end
        
        function ldaMatrix = generateConfusionMatrix(obj,testingSet,predictedVals) % makes custom confusion matrix
            %its 'normalised' so that all of the little boxes add to 1
            ldaMatrix = zeros(length(obj.arrayOfLabels),length(obj.arrayOfLabels));
            exampleTotalInTests = size(predictedVals,1);
            for outputIterator = 1:exampleTotalInTests
                previousValueInMatrix = ldaMatrix(predictedVals(outputIterator),testingSet(outputIterator,(obj.numberOfFeatures+1)));
                ldaMatrix(predictedVals(outputIterator),testingSet(outputIterator,(obj.numberOfFeatures+1))) = previousValueInMatrix + (1);
            end
        end
        
        
        function obj = generateAverageAccuracy(obj, matrixIn)%creates the average accuracy metric, basically the sum of all of the diagonal values
            foldWidth = floor(size(obj.threeDimArrayOfGrasps,1)/obj.numberOfKfolds);
            for matrixIterator = 1:size(matrixIn,1)
                averageAccuracyPrev = obj.averageAccuracy;
                obj.averageAccuracy = (matrixIn(matrixIterator,matrixIterator)/(foldWidth + 1)) + double(averageAccuracyPrev)/foldWidth;
            end
        end
        
        function averageAccuracyOut = returnAverageAccuracy(obj)%returns the average accuracy value
            averageAccuracyOut = obj.averageAccuracy;
        end
        
        function featureRanksOut = returnFeatureRanks(obj)% returns the feature ranks array (average of all folds)
            featureRanksOut = obj.RFFeatureRanks;
        end
    end
end