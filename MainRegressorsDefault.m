clear;
close all;
selectStagesToRun = [true,true,true]; %boolean array the size of the total number of stages in the program, decides which stages to run
% ! make sure to generate grasp files before doing anything else
% stage 2 should run if you want to run any other later stages as it
% generates the 3D array processed in later functions
arrayOfSubjects = {'AD','CC','CT','CY'};
arrayOfObjects = {'Cube','Cylinder','OffCenterCube'};
arrayOfMotions = {'Pitch','Roll','Yaw'};
numberOfTrials = 10; %max number of trials for subjects

% control options for first stage (labelled CSV -> 3D organised array of different groups) 
desiredNumberOfTrials = 100000;% good for ninapro with 1 subject%194187 good for first dataset;%324966; % how many rows or data points we want to feed the classifiers should be as big as the smallest number of rows of a single test file multiplied by the number of trials taken
numberOfChannels = 16; % number of EMG channels which to the classifiers is the number of features has to be the same or less than the largest number of features in the file
separateByInformation = 3; % separates the regressions to be either subject specific(1), object specific(2), motion specific(3) or generic (0) of these each test set becomes a layer
restLength = 4; % length of rest period at the start of each trial in seconds
removeEndLength = 1; %removes part of each trial at the end as there is an issue at the end of the recording

% control options for second stage filters are applied at this stage
order = 4; %For Savitsky Golay filtering
framelength = 21; %For Savitsky Golay filtering filtering-must be odd number
movingAverageLength = 200; %for moving average filter
whichFilter = 3; % 1 = moving average, 2 = savitsky golay filter
stepSize = 1; %Anany's Filter
windowSlide = 5; %Anany's Filter
isRegression = true; %Anany's filter- will ignore the first 6 columns
% control options for fourth stage (apply classifiers and output confusion matrix)
kFoldsForCrossValidation = 5; %currently set to 5, 5 fold cross validation
SelectRegressorsToRun = [true,true,true,true]; % select which classifiers to run: [Linear,SVR,RF,NN]

% hyperparameter selection for regressors:
numberOfTrees = 10; % number of trees hyperparameter for random forest classification, works well rn with 5 but apparently RF cannot be overfit so can have many trees (although takes ages)
%https://stats.stackexchange.com/questions/36165/does-the-optimal-number-of-trees-in-a-random-forest-depend-on-the-number-of-pred
numberOfHiddenNeurons = 8; % according to this page the number of neurons should be a number ranging from the number of inputs (8) and outputs (1)
%https://stats.stackexchange.com/questions/181/how-to-choose-the-number-of-hidden-layers-and-nodes-in-a-feedforward-neural-netw

%% Run Stage 1
if selectStagesToRun(1) == true % converts to a 3D array of regression data
    
    if separateByInformation == 0
        arrayOfRegressionDepth = 1;
        labelForLayer = {'general'};
    elseif separateByInformation == 1
        arrayOfRegressionDepth = length(arrayOfSubjects);
        labelForLayer = arrayOfSubjects;
    elseif separateByInformation == 2
        arrayOfRegressionDepth = length(arrayOfObjects);
        labelForLayer = arrayOfObjects;
    elseif separateByInformation == 3
        arrayOfRegressionDepth = length(arrayOfMotions);
        labelForLayer = arrayOfMotions;
    end

    arrayOfRegressionData = zeros(desiredNumberOfTrials,(numberOfChannels+6),arrayOfRegressionDepth); %unfolded grasp type array
    startIndex = 1;
    endIndex = 0;
    tooLong = false;
    %makes 3D array of row length of desired number of trials and if chosen to have either subject/ object/ motion specific arrangement each 'layer' is one of each (defined at the top in the user inputs)
    for subjectIterator = 1:length(arrayOfSubjects)
        for objectIterator = 1:length(arrayOfObjects)
            for motionIterator = 1:length(arrayOfMotions)
                for trialIterator = 1:numberOfTrials
                    path = strcat('Regression\',arrayOfSubjects(subjectIterator),'\',arrayOfObjects(objectIterator),'\',arrayOfMotions(motionIterator));
                    
                    disp(strcat(path,'\T',num2str(trialIterator),'RR.csv'));
                    if ~isfile(strcat(path,'\T',num2str(trialIterator),'RR.csv'))
                        break;
                    end
                    
                    singleTrialArray = csvread(strcat(path,'\T',string(trialIterator),'RR.csv'),2);
                    singleTrialLength = size(singleTrialArray,1)- removeEndLength*1200;
                    endIndex = endIndex + singleTrialLength - restLength*1200;
                    
                    if tooLong == false
                        
                        if endIndex > desiredNumberOfTrials
                            singleTrialLength = singleTrialLength - (endIndex - desiredNumberOfTrials);
                            endIndex = desiredNumberOfTrials;
                            tooLong = true;
                        end
                        
                        for motionRecordingIterator = 1:6
                            restAverage = mean(singleTrialArray(1:(restLength*1200),motionRecordingIterator));
                            singleTrialArray((restLength*1200+1):singleTrialLength,motionRecordingIterator) = singleTrialArray((restLength*1200+1):singleTrialLength,motionRecordingIterator)- restAverage;
                        end   
                    
                        if separateByInformation == 0
                            arrayOfRegressionData(startIndex:endIndex,1:(6+numberOfChannels),1) = singleTrialArray((restLength*1200+1):singleTrialLength,1:(6+numberOfChannels));
                        elseif separateByInformation == 1
                            arrayOfRegressionData(startIndex:endIndex,1:(6+numberOfChannels),subjectIterator) = singleTrialArray((restLength*1200+1):singleTrialLength,1:(6+numberOfChannels));
                        elseif separateByInformation == 2
                            arrayOfRegressionData(startIndex:endIndex,1:(6+numberOfChannels),objectIterator) = singleTrialArray((restLength*1200+1):singleTrialLength,1:(6+numberOfChannels));
                        elseif separateByInformation == 3
                            arrayOfRegressionData(startIndex:endIndex,1:(6+numberOfChannels),motionIterator) = singleTrialArray((restLength*1200+1):singleTrialLength,1:(6+numberOfChannels));
                        end
                        startIndex = endIndex + 1;

                    end 
                end
                startIndex = 1;
                endIndex = 0;
                tooLong = false;
            end
        end
    end
    
end

%% Run Stage 2
if selectStagesToRun(2) == true % apply a filter and make the EMG readings into magnitude rather than magnitude and sign
    szdim3 = size(arrayOfRegressionData,3); 
    vectorV = ones(1,movingAverageLength)/movingAverageLength; %moving average vector for convolution with data.
    %totalArrayOfRegressionDataNew = double.empty((size(arrayOfRegressionData,1)./windowSlide-round(movingAverageLength/windowSlide)),(numberOfChannels+6),0);%(S/WindowSlide) - (WindowSize/WindowSlide)
    if whichFilter == 1
        %Moving Average Filter attempt
        for iterationFilter = 1:szdim3movingAverageLength
             filteredData = Filters(order, framelength, iterationFilter, numberOfChannels, vectorV, movingAverageLength);
             arrayOfRegressionData(:,7:(numberOfChannels+6),iterationFilter) = filteredData.movingAverage(abs(arrayOfRegressionData(:,7:(numberOfChannels+6),iterationFilter)));
        end
    
    elseif whichFilter == 2
        %Applies SG filter
        for iterationFilter = 1:szdim3
            filteredData = Filters( order, framelength, iterationFilter, numberOfChannels, vectorV);
            arrayOfRegressionData(:,7:(numberOfChannels+6),iterationFilter) = filteredData.SavitzkyGFilter(abs(arrayOfRegressionData(:,7:(numberOfChannels+6),iterationFilter)));
        end
    elseif whichFilter == 3
        for iterationFilter = 1:szdim3
            filteredData = Filters( order, framelength, iterationFilter, (numberOfChannels+6), vectorV, movingAverageLength,stepSize,windowSlide);
            [arrayOfRegressionMotions(:,1:6,iterationFilter),arrayOfRegressionDataNew(:,1:numberOfChannels,iterationFilter)] = filteredData.TimeDomainFE_Ver3(isRegression, arrayOfRegressionData(:,:,iterationFilter));
        end
        arrayOfRegressionData = [arrayOfRegressionMotions,arrayOfRegressionDataNew];
    end
    
end
%% Run Stage 3
if selectStagesToRun(3) == true % apply selected regressors and output their performance
    % create classifier instance
    regressorInstanceLR = Regressors(arrayOfRegressionData, labelForLayer,kFoldsForCrossValidation, numberOfChannels);
    regressorInstanceSVR = Regressors(arrayOfRegressionData, labelForLayer,kFoldsForCrossValidation, numberOfChannels);
    regressorInstanceRF = Regressors(arrayOfRegressionData, labelForLayer,kFoldsForCrossValidation, numberOfChannels);
    regressorInstanceNN = Regressors(arrayOfRegressionData, labelForLayer,kFoldsForCrossValidation, numberOfChannels);
    
    foldIndexOfGrasps = regressorInstanceLR.produceCrossValidationFoldIndex();

    % one fold is selected as the testing fold and the remainder folds are
    % selected as the training folds
    trainingGroup = double.empty(0,(numberOfChannels+7));
    figureNames = {'f1','f2','f3','f4','f5'};
    LRaccuracy = zeros(30,size(arrayOfRegressionData,3));
    NNaccuracy = zeros(30,size(arrayOfRegressionData,3));
    RFaccuracy = zeros(30,size(arrayOfRegressionData,3));
    SVRaccuracy = zeros(30,size(arrayOfRegressionData,3));
    
    
 for layerIterator = 1:size(arrayOfRegressionData,3)   
    for testFoldSelected = 1:kFoldsForCrossValidation        
        figureNames{testFoldSelected} = figure;
        testGroup = arrayOfRegressionData(foldIndexOfGrasps(1,testFoldSelected):foldIndexOfGrasps(2,testFoldSelected),:,layerIterator);
        time = linspace(0,(size(testGroup,1)/1200),size(testGroup,1));
        for trainingFoldIterator = 1:kFoldsForCrossValidation
            if(trainingFoldIterator ~= testFoldSelected)
                trainingGroup = [trainingGroup;arrayOfRegressionData(foldIndexOfGrasps(1,trainingFoldIterator):foldIndexOfGrasps(2,trainingFoldIterator),:,layerIterator)];
            end
        end
        
        trainingFoldCount = 0;
        %apply regressors here
        if SelectRegressorsToRun(1) == 1 % Runs Linear regression and outputs a plot of predicted and collected data
            LRplot = regressorInstanceLR.LinearRegressor(trainingGroup, testGroup);
            LRfit_nmse = goodnessOfFit(LRplot,testGroup(:,1:6),'NMSE');
            LRaccuracy((1 + 6*(testFoldSelected - 1)):(6 + 6*(testFoldSelected - 1)),layerIterator) = LRfit_nmse;
            
            for plotIterate = 1:6
                if plotIterate < 4
                    subplot(2,4,1);
                    y = LRplot(:,plotIterate);
                    yTest = testGroup(:,plotIterate);
                    plot(time,y,time,yTest,'--');
                    hold on;
                end
                if plotIterate == 4
                    legend('x fit','x','y fit', 'y', 'z fit', 'z');
                    title('Linear fit for rotations');
                    hold off;
                end
                if plotIterate >= 4
                    subplot(2,4,2);
                    y = LRplot(:,plotIterate);
                    yTest = testGroup(:,plotIterate);
                    plot(time,y,time,yTest,'--');
                    hold on;
                end
            end
            legend('x fit','x','y fit', 'y', 'z fit', 'z');
            title('Linear fit for translations');
            hold off;
        end
        
        if SelectRegressorsToRun(2) == 1 %Runs suppport vector regressor and outputs a confusion matrix
            SVROutput = regressorInstanceSVR.SupportVectorRegressor(trainingGroup, testGroup);
            SVRfit_nmse = goodnessOfFit(SVROutput,testGroup(:,1:6),'NMSE');
            SVRaccuracy((1 + 6*(testFoldSelected - 1)):(6 + 6*(testFoldSelected - 1)),layerIterator) = SVRaccuracy((1 + 6*(testFoldSelected - 1)):(6 + 6*(testFoldSelected - 1)),layerIterator) + SVRfit_nmse*(1/kFoldsForCrossValidation)*(1/layerIterator);
            
            for plotIterate = 1:6
                if plotIterate < 4
                    subplot(2,4,3);
                    y = SVROutput(:,plotIterate);
                    yTest = testGroup(:,plotIterate);
                    plot(time,y,time,yTest,'--');
                    hold on;
                end
                if plotIterate == 4
                    legend('x fit','x','y fit', 'y', 'z fit', 'z');
                    title('SVR fit for rotations');
                    hold off;
                end
                if plotIterate >= 4
                    subplot(2,4,4);
                    y = SVROutput(:,plotIterate);
                    yTest = testGroup(:,plotIterate);
                    plot(time,y,time,yTest,'--');
                    hold on;
                end
            end
            legend('x fit','x','y fit', 'y', 'z fit', 'z');
            title('SVR fit for translations');
            hold off;
        end
        
        if SelectRegressorsToRun(3) == 1 %Runs RF classifier and outputs a confusion matrix (quite slow with only 5 trees, a few mins if you want 50 trees (I got about 0.9838 accuracy with 50) although pretty good with 5 trees)
            [RFOutput,regressorInstanceRF] = regressorInstanceRF.RandomForestRegressor(trainingGroup, testGroup, numberOfTrees);
            RFfit_nmse = goodnessOfFit(RFOutput,testGroup(:,1:6),'NMSE');
            RFaccuracy((1 + 6*(testFoldSelected - 1)):(6 + 6*(testFoldSelected - 1)),layerIterator) = RFaccuracy((1 + 6*(testFoldSelected - 1)):(6 + 6*(testFoldSelected - 1)),layerIterator) + RFfit_nmse*(1/kFoldsForCrossValidation)*(1/layerIterator);
            
            for plotIterate = 1:6
                if plotIterate < 4
                    subplot(2,4,5);
                    y = RFOutput(:,plotIterate);
                    yTest = testGroup(:,plotIterate);
                    plot(time,y,time,yTest,'--');
                    hold on;
                end
                if plotIterate == 4
                    legend('x fit','x','y fit', 'y', 'z fit', 'z');
                    title('RF fit for rotations');
                    hold off;
                end
                if plotIterate >= 4
                    subplot(2,4,6);
                    y = RFOutput(:,plotIterate);
                    yTest = testGroup(:,plotIterate);
                    plot(time,y,time,yTest,'--');
                    hold on;
                end 
            end
            legend('x fit','x','y fit', 'y', 'z fit', 'z');
            title('RF fit for translations');
            hold off;
            
            
        end
        
        if SelectRegressorsToRun(4) == 1 %Runs NN classifier and outputs a confusion matrix (even slower to output, you have to wait a few mins has a little window to show it's progress when running)
            NNplot = regressorInstanceNN.NeuralNetworkRegressor(trainingGroup,testGroup,numberOfHiddenNeurons);
            NNfit_nmse = goodnessOfFit(transpose(NNplot),testGroup(:,1:6),'NMSE');
            NNaccuracy((1 + 6*(testFoldSelected - 1)):(6 + 6*(testFoldSelected - 1)),layerIterator) = NNaccuracy((1 + 6*(testFoldSelected - 1)):(6 + 6*(testFoldSelected - 1)),layerIterator) + NNfit_nmse*(1/kFoldsForCrossValidation)*(1/layerIterator);
            
            for plotIterate = 1:6
                if plotIterate < 4
                    subplot(2,4,7);
                    y = transpose(NNplot(plotIterate,:));
                    yTest = testGroup(:,plotIterate);
                    plot(time,y,time,yTest,'--');
                    hold on;
                end
                if plotIterate == 4
                    legend('x fit','x','y fit', 'y', 'z fit', 'z');
                    title('NN fit for rotations');
                    hold off;
                end
                if plotIterate >= 4
                    subplot(2,4,8);
                    y = transpose(NNplot(plotIterate,:));
                    yTest = testGroup(:,plotIterate);
                    plot(time,y,time,yTest,'--');
                    hold on;
                end
            end
            legend('x fit','x','y fit', 'y', 'z fit', 'z');
            title('NN fit for translations');
            hold off;
        end
        SaveTraining = trainingGroup;
        trainingGroup = double.empty(0,(numberOfChannels+7));

    end
 end
end

%plots feature ranks for random forest, shows which emg signals were most
%useful in making the regression model
RFFeatureRanksTotal = regressorInstanceRF.returnFeatureRanks();

for motionFeatureRankingIterator = 1:6
    figureRankings = figure;
    bar(RFFeatureRanksTotal(:,motionFeatureRankingIterator));
    title('EMG channel readings relating to hand gestures');
    ylabel('Predictor importance estimates');
    xlabel('Predictors');
    h = gca;
    h.XTickLabelRotation = 45;
    h.TickLabelInterpreter = 'none';
end
