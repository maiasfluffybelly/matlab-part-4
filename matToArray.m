
classdef matToArray
    
    properties
        subjectIndex
        graspIndex
        subjects %array of subject names
        grasps %array of grasp names
        trials %number of trials
        fileName
        twoDimensionalArray
        listPairArray
        graspArray
        restArray
        letter
        exerciseIndex
        columns
        exercises
        graspNumber
        graspList
        graspIndexCount
    end
    
    properties (Dependent)
        RearrangedFile
    end
    methods
        
        function self = matToArray(subjectIndex,exerciseIndex,subjects,exercises,columns, graspNumber)
            self.subjectIndex = subjectIndex;
            self.exerciseIndex = exerciseIndex;
            self.subjects = subjects;
            self.exercises = exercises;
           % self.grasps = grasps;
            %self.letter = letter;
            %self.trials = trials;
            self.columns = columns;
            self.graspNumber = graspNumber;
            self.twoDimensionalArray = zeros(10,9);
        end
         
        function self = arraySort(self) 
            disp(self.twoDimensionalArray)
            filename = (strcat(self.subjects(self.subjectIndex),{'_A1_'},string(self.exercises(self.exerciseIndex)),{'.mat'}));
            disp(filename);
            load(filename,'emg','restimulus')
            self.twoDimensionalArray = emg; 
            self.twoDimensionalArray(:,(self.columns+1)) = restimulus;
        end
  

%         function self = organiseByTriggers(self)
%             self.indexingStart = zeros(1,1);
%             self.indexingEnd = zeros(1,1);
%             A = self.twoDimensionalArray;
%             currentGraspNo = 100;
%             previousGrasps = zeros(1,1);
%             if isempty(A) == false
%                 for currentRow = 1:length(A)
%                     currentGraspNo = A(currentRow,(self.columns+1));
%                        if currentGraspNo ~= lastGraspNo
%                            %check if had grasp before
%                            for interator = 1:length(previousGrasps)
%                                if previousGrasps(iterator,1) == currentGraspNo
%                                    similar = previousGrasps(iterator,1);
%                                    previousGrasps(iterator,2) = previousGrasps(iterator,2) +1;
%                                end
%                            end
%                            %Start new row of indices for grasp
%                            if similar == 0
%                                previousGrasps(currentGraspNo,2) = 1;
%                            end
%                            %Next start
%                            self.indexingStart(similar,previousGrasps(similar,2))  = currentRow;
%                            %Previous end
%                            self.indexingEnd(currentGrasp,previousGrasps(similar,2)) = currentRow-1;
%                        end
%                 end
%             end
%       end
% 
%         
%         function self = RearrangeNewData(self)
%             for currentGrasp = 1:length(self.indexingStart)+1
%                 graspFileName = char(strcat(cellstr(num2str(currentGrasp)),'_graspNina.csv'));
%                 disp(graspFileName)
%                            
% %                 self.graspArray = int64.empty(0,8);
% %                 self.restArray = int64.empty(0,8);
%                 disp(length(self.listPairArray));
%                 for listPairIndex = 2:(length(self.listPairArray)-2) %using the list pair index information the alternating rest and grasp phases are taken out of twoDimensionalArray
%                     if mod(listPairIndex,2) == 0 
%                         self.graspArray = [self.graspArray; self.twoDimensionalArray((self.listPairArray(listPairIndex,1)):(self.listPairArray(listPairIndex,2)),1:8)];
%                     else
%                         self.restArray = [self.restArray; self.twoDimensionalArray((self.listPairArray(listPairIndex,1)):(self.listPairArray(listPairIndex,2)),1:8)];
%                     end
%                 end
%                 
% %                 if currentGrasp == 1
% %                     start = 1;
% %                 else
% %                     disp(currentGrasp)
% %                     start = self.indexingStart(currentGrasp-1)+1;
% %                 end
%                
%                 self.graspArray = self.twoDimensionalArray(start:self.indexing(currentGrasp),self.columns);
%                 if isfile(graspFileName)% if the grasp file already exists-append, if it does not make a new file
%                     dlmwrite(graspFileName,self.graspArray,'delimiter',',','-append');
%                 else
%                     dlmwrite(graspFileName,self.graspArray,'delimiter',',');
%                 end
%             end
% 
%                 self.graspArray = [];
%         end
%     end
% end

        function self = organiseByTriggers(self)
            %3D Array to collect data from csv files and concatanate them. 
            %Takes 9 by however many rows,
            %selects indexers accroding to triggers...(3rd dimension)
            %new start = current+1 row, clomun 1. 
            self.listPairArray = int64.empty(0,0);
            %self.graspList = int64.empty(0);
            startRow = 1;
            currentGrasp = 0;
            self.graspIndexCount = 1;
            A = self.twoDimensionalArray;
            if isempty(A) == false
                for indexRow = 1:length(A)
                    triggerState = A(indexRow,self.columns+1);
                    if triggerState ~= currentGrasp
                        endRow = indexRow-1;
                        currentGrasp = triggerState;
                        self.listPairArray = [self.listPairArray ; [startRow,endRow]]; 
                        self.graspList(self.graspIndexCount) = triggerState;
                        self.graspIndexCount = self.graspIndexCount +1;
                        startRow = indexRow;
                    end

                end 
            end
        end
        
        function self = RearrangeNewData(self, rearrangeBy)
            if rearrangeBy == 1% 1 is selected to rearrange the files by grasp types only potential for other rearranging options
                self.graspArray = int64.empty(0,8);
                disp(length(self.listPairArray));
                for listPairIndex = 1:length(self.listPairArray)
                    self.graspArray = [self.twoDimensionalArray((self.listPairArray(listPairIndex,1)):(self.listPairArray(listPairIndex,2)),1:self.columns)];
                    graspFileName = char(strcat('Subject_', num2str(self.subjectIndex),'_E',num2str(self.exerciseIndex),'_grasp' ,num2str(self.graspList(listPairIndex)), '.csv'));

                    if isfile(graspFileName)% if the grasp file already exists-append, if it does not make a new file
                        dlmwrite(graspFileName,self.graspArray,'delimiter',',','-append');
                    else
                        dlmwrite(graspFileName,self.graspArray,'delimiter',',');
                    end
                end
                
                self.graspArray = [];
            end
        end
    end
    
end



   



     