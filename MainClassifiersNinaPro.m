clear;
close all;
selectStagesToRun = [false,true,true,true]; %boolean array the size of the total number of stages in the program, decides which stages to run
% ! make sure to generate grasp files before doing anything else
% stage 2 should run if you want to run any other later stages as it
% generates the 3D array processed in later functions

% parameters for NinaPro dataset
arrayOfSubjectsNinaPro = {'S1'};
arrayOfExercisesNinaPro = {'E1','E2','E3'};
arrayOfGraspsPerExerciseNinaPro = [12,17,23];


%% control options for first stage (experimental test CSV -> labelled CSV for classifier) ! run this once or else it will keep adding to existing files
missalignmentOffset = 6; %delay during grasp recordings, cuts out dynamic part of grasp in hundreds of milliseconds
rearrangeBy = 1; % if 1 is selected the file rearranger will rearrange the files into large arrays of grasps only
equalSize = true; % make sure that each rest and grasp test is the same number of rows

%% control options for second stage (labelled CSV -> 3D organised array of different groups) 
sequenceType = [0,1]; % rest = 0 and grasp = 1
restSelectedInSequence = 1; % pick which rest from rest grasp set to be used in the classifier code
desiredNumberOfTrials = 4000;% good for ninapro with 1 subject%194187 good for first dataset;%324966; % how many rows or data points we want to feed the classifiers should be as big as the smallest number of rows of a grasp file
%desiredNumberOfTrials = 2000;% good for ninapro with 1 subject%194187 good for first dataset;%324966; % how many rows or data points we want to feed the classifiers should be as big as the smallest number of rows of a grasp file
numberOfChannels = 10; % number of EMG channels which to the classifiers is the number of features has to be the same or less than the largest number of features in the file
numberOfClasses = 10; % optional lower number of classes, otherwise set this to -1

%% control options for third stage filters are applied at this stage
order = 4; %For Savitsky Golay filtering
framelength = 21; %For Savitsky Golay filtering filtering-must be odd number
movingAverageLength = 21;
whichFilter = 3; % 1 = moving average, 2 = savitsky golay filter 3 = Anany's filter
stepSize = 1; %Anany's Filter
windowSlide = 5; %Anany's Filter
isRegression = false; %Anany's filter- will ignore the first 6 columns

%% control options for fourth stage (apply classifiers and output confusion matrix)
kFoldsForCrossValidation = 5; %currently set to 5, 5 fold cross validation
SelectClassifiersToRun = [true,false,false,false,false]; % select which classifiers to run: [LDA,KNN,RF,NN,SVM]

% hyperparameter selection for classifiers:
numberOfTrees = 5; % number of trees hyperparameter for random forest classification, works well rn with 5 but apparently RF cannot be overfit so can have many trees (although takes ages)
%https://stats.stackexchange.com/questions/36165/does-the-optimal-number-of-trees-in-a-random-forest-depend-on-the-number-of-pred
numberOfNeighbours = 221; % K value is N^(1/2)/2 N = Number of training Samples, odd value suggested online:
%https://stackoverflow.com/questions/11568897/value-of-k-in-k-nearest-neighbor-algorithm
numberOfHiddenNeurons = 8; % according to this page the number of neurons should be a number ranging from the number of inputs (8) and outputs (1)
%https://stats.stackexchange.com/questions/181/how-to-choose-the-number-of-hidden-layers-and-nodes-in-a-feedforward-neural-netw

if selectStagesToRun(1) == true % reads from the csv/ mat files and converts them into grasp related files
   for subjectIndex = 1:length(arrayOfSubjectsNinaPro)
        for exerciseIndex = 1:length(arrayOfExercisesNinaPro)
                newdataset = NinaProToArray(subjectIndex,exerciseIndex,arrayOfSubjectsNinaPro,arrayOfExercisesNinaPro,10,sum(arrayOfGraspsPerExerciseNinaPro)); %create instance of class FileToDataset
                newdataset = newdataset.arraySort();
                newdataset = newdataset.organiseByTriggers();
                newdataset = newdataset.RearrangeNewData(1);
        end
   end
end



arrayOfGraspTypes = double.empty(desiredNumberOfTrials,numberOfChannels,0); %unfolded grasp type array
graspLabelForLayer = {};

if selectStagesToRun(2) == true % converts to a 3D array of grasps
    % this will force the length of the grasp arrays to be equal so that the classifer is not biased towards a class
    
    % set the file inputs for the methods in the DatasetToArray class to be compatable with NinaPro data
    sequenceTypeIndex = 0;
    arrayOfGrasps = arrayOfExercisesNinaPro;
    arrayOfSubjects = arrayOfSubjectsNinaPro;
    
    for graspIndex = 1:length(arrayOfGraspsPerExerciseNinaPro)
        for graspPerExerciseIterate = 1:arrayOfGraspsPerExerciseNinaPro(graspIndex)+1
            if (graspPerExerciseIterate > 1 || graspIndex == restSelectedInSequence)% removes the 'rests' that are not the one selected by user
                sequenceTypeIndex = sequenceTypeIndex + 1;
                sequenceType(sequenceTypeIndex) = graspPerExerciseIterate-1;
            end
        end

    end
    

    %initialising variables used in this loop
    iteration = 0;
    
    counter = 0;
    for graspIndex = 1:length(arrayOfGrasps)
        sequenceLength = arrayOfGraspsPerExerciseNinaPro(graspIndex)+1;        
        for sequenceTypeIndex = 1:sequenceLength % makes sure we only add one file of 'rest type'
            if ((iteration < numberOfClasses) || (numberOfClasses == -1)) && (sequenceTypeIndex > 1 || graspIndex == restSelectedInSequence) % limits number of classes if desired
                iteration = iteration + 1;
                arrayData = DatasetToArray(graspIndex,arrayOfGrasps,sequenceType,sequenceTypeIndex); %create instance of class FileToDataset
                arrayOfGraspTypes = arrayData.GraspCsvToArrayNinaPro(iteration,desiredNumberOfTrials,arrayOfGraspTypes, numberOfChannels);
                graspLabelForLayer = arrayData.applyArrayLabelsNinaPro(graspLabelForLayer, iteration);
            end
        end    
        
    end
end

if selectStagesToRun(3) == true % apply a filter and make the EMG readings into magnitude rather than magnitude and sign
    szdim3 = size(arrayOfGraspTypes,3); 
    vectorV = ones(1,movingAverageLength)/movingAverageLength; %moving average vector for convolution with data.
     
    if whichFilter == 1
        %Moving Average Filter attempt
        for iterationFilter = 1:szdim3
             filteredData = Filters(order, framelength, iterationFilter, numberOfChannels, vectorV, movingAverageLength);
             arrayOfGraspTypes(:,:,iterationFilter) = filteredData.movingAverage(abs(arrayOfGraspTypes(:,:,iterationFilter)));
        end
    
    elseif whichFilter == 2
        %Applies SG filter
        %test = abs(arrayData.outputArray());
        for iterationFilter = 1:szdim3
            filteredData = Filters( order, framelength, iterationFilter, numberOfChannels, vectorV);
            arrayOfGraspTypes(:,:,iterationFilter) = filteredData.SavitzkyGFilter(abs(arrayOfGraspTypes(:,:,iterationFilter)));
        end
    elseif whichFilter == 3
        for iterationFilter = 1:szdim3
            filteredData = Filters( order, framelength, iterationFilter, numberOfChannels, vectorV, movingAverageLength,stepSize,windowSlide);
           % timeDomainFeatures = TimeDomainFE_Ver3(arrayOfGraspTypes(:,:,iterationFilter),1,1,200,10,1,8);
            [arrayOfNothing,arrayOfGraspTypesNew(:,:,iterationFilter)] = filteredData.TimeDomainFE_Ver3(isRegression,(arrayOfGraspTypes(:,:,iterationFilter)));
        end
        arrayOfGraspTypes = arrayOfGraspTypesNew;
    end


end

if selectStagesToRun(4) == true % apply selected classifiers and output their performance
    % create classifier instance
    classifierInstanceLDA = Classifiers(arrayOfGraspTypes, graspLabelForLayer,kFoldsForCrossValidation, numberOfChannels);
    classifierInstanceKNN = Classifiers(arrayOfGraspTypes, graspLabelForLayer,kFoldsForCrossValidation, numberOfChannels);
    classifierInstanceRF = Classifiers(arrayOfGraspTypes, graspLabelForLayer,kFoldsForCrossValidation, numberOfChannels);
    classifierInstanceNN = Classifiers(arrayOfGraspTypes, graspLabelForLayer,kFoldsForCrossValidation, numberOfChannels);
    classifierInstanceSVM = Classifiers(arrayOfGraspTypes, graspLabelForLayer,kFoldsForCrossValidation, numberOfChannels);
    
    foldIndexOfGrasps = classifierInstanceLDA.produceCrossValidationFoldIndex();

    % one fold is selected as the testing fold and the remainder folds are
    % selected as the training folds
    trainingGroup = double.empty(0,(numberOfChannels+1));

    figureNames = {'f1','f2','f3','f4','f5'};
    
    for testFoldSelected = 1:kFoldsForCrossValidation
        figureNames{testFoldSelected} = figure;
        testGroup = classifierInstanceLDA.Convert3DarrayInto2DLabelledArray(arrayOfGraspTypes(foldIndexOfGrasps(1,testFoldSelected):foldIndexOfGrasps(2,testFoldSelected),:,1:length(graspLabelForLayer)));
        
        for trainingFoldIterator = 1:kFoldsForCrossValidation
            if(trainingFoldIterator ~= testFoldSelected)
                trainingGroup = [trainingGroup;classifierInstanceLDA.Convert3DarrayInto2DLabelledArray(arrayOfGraspTypes(foldIndexOfGrasps(1,trainingFoldIterator):foldIndexOfGrasps(2,trainingFoldIterator),:,1:length(graspLabelForLayer)))];
            end
        end
        
        trainingFoldCount = 0;
        %apply classifiers here
        if SelectClassifiersToRun(1) == 1 % Runs LDA classifier and outputs a confusion matrix
            ldaOutput = classifierInstanceLDA.LinearDiscriminantAnalyser(trainingGroup, testGroup);
            ldaMatrix = classifierInstanceLDA.generateConfusionMatrix(testGroup,ldaOutput);
            subplot(2,3,1);
            title('LDA');
            ldaMatrix=confusionmat(testGroup(:,numberOfChannels+1),ldaOutput);
            confusionchart(figureNames{testFoldSelected},int64(ldaMatrix),transpose(graspLabelForLayer));
            classifierInstanceLDA = classifierInstanceLDA.generateAverageAccuracy(ldaMatrix);
        end
        
        if SelectClassifiersToRun(2) == 1 %Runs KNN classifier and outputs a confusion matrix
            KNNOutput = classifierInstanceKNN.KNearestNeighboursClassifier(trainingGroup, testGroup, numberOfNeighbours);
            KNNMatrix = classifierInstanceKNN.generateConfusionMatrix(testGroup,KNNOutput);
            subplot(2,3,2);
            title('KNN');
            confusionchart(figureNames{testFoldSelected},int64(KNNMatrix),transpose(graspLabelForLayer));
            classifierInstanceKNN = classifierInstanceKNN.generateAverageAccuracy(KNNMatrix);
        end
        
        if SelectClassifiersToRun(3) == 1 %Runs RF classifier and outputs a confusion matrix (quite slow with only 5 trees, a few mins if you want 50 trees (I got about 0.9838 accuracy with 50) although pretty good with 5 trees)
            [RFOutput,classifierInstanceRF] = classifierInstanceRF.RandomForestClassifier(trainingGroup, testGroup, numberOfTrees);
            RFMatrix = classifierInstanceRF.generateConfusionMatrix(testGroup,str2double(RFOutput));
            subplot(2,3,3);
            title('RF');
            confusionchart(figureNames{testFoldSelected},int64(RFMatrix),transpose(graspLabelForLayer));
            classifierInstanceRF = classifierInstanceRF.generateAverageAccuracy(RFMatrix);
        end
        
        if SelectClassifiersToRun(4) == 1 %Runs NN classifier and outputs a confusion matrix (even slower to output, you have to wait a few mins has a little window to show it's progress when running)
            NNOutput = classifierInstanceNN.NeuralNetworkClassifier(trainingGroup,testGroup,numberOfHiddenNeurons);
            NNMatrix = classifierInstanceNN.generateConfusionMatrix(testGroup,NNOutput);
            subplot(2,3,4);
            title('NN');
            confusionchart(figureNames{testFoldSelected},int64(NNMatrix),transpose(graspLabelForLayer));
            classifierInstanceNN = classifierInstanceNN.generateAverageAccuracy(NNMatrix);
        end
        
        if SelectClassifiersToRun(5) == 1%Runs SVM classifier and outputs a confusion matrix
            SVMOutput = classifierInstanceSVM.SupportVectorMachineClassifier(trainingGroup, testGroup);
            SVMMatrix = classifierInstanceSVM.generateConfusionMatrix(testGroup,SVMOutput);
            subplot(2,3,5);
            title('SVM');
            confusionchart(figureNames{testFoldSelected},int64(SVMMatrix),transpose(graspLabelForLayer));
            classifierInstanceSVM = classifierInstanceSVM.generateAverageAccuracy(SVMMatrix);
        end
        trainingGroup = double.empty(0,(numberOfChannels+1));

    end
    
    ldaAverageAccuracy = classifierInstanceLDA.returnAverageAccuracy();
    KNNAverageAccuracy = classifierInstanceKNN.returnAverageAccuracy();
    RFAverageAccuracy = classifierInstanceRF.returnAverageAccuracy();
    RFFeatureRanksTotal = classifierInstanceRF.returnFeatureRanks();
    
    figureRankings = figure;
    bar(RFFeatureRanksTotal);
    title('EMG channel readings relating to hand gestures');
    ylabel('Predictor importance estimates');
    xlabel('Predictors');
    h = gca;
    h.XTickLabelRotation = 45;
    h.TickLabelInterpreter = 'none';
    NNAverageAccuracy = classifierInstanceNN.returnAverageAccuracy();
    SVMAverageAccuracy = classifierInstanceSVM.returnAverageAccuracy();
    
    disp(ldaAverageAccuracy);
    disp(KNNAverageAccuracy);
    disp(RFAverageAccuracy);
    disp(NNAverageAccuracy);
    disp(SVMAverageAccuracy);
end