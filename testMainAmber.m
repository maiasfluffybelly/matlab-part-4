clear;


arrayOfSubjects = {'S1'};
arrayOfExercises = {'E1','E2','E3'};
numberOfColumns = 10;
graspNumber = 52;


    %the main pipeline to create grasp files from subject tests
    for subjectIndex = 1:length(arrayOfSubjects)
        for exerciseIndex = 1:length(arrayOfExercises)
                newdataset = matToArray(subjectIndex,exerciseIndex,arrayOfSubjects,arrayOfExercises,numberOfColumns,graspNumber); %create instance of class FileToDataset
                newdataset = newdataset.arraySort();
                newdataset = newdataset.organiseByTriggers();
                newdataset = newdataset.RearrangeNewData(1);
        end
    end
