classdef Filters
    
     properties
        order %Filter order
        framelength %desired input for framelength
        iteration %current loop iteration from main
        columnNo %number of columns needed
        twoDMovingAv
        threeDMovingAverage
        vectorV %moving average vector for convolution with data.
        movingAverageLength
        stepSize
        windowSlide
    end

    methods
        
        function obj = Filters(order, framelength, iteration, columnNo, vectorV, movingAverageLength, stepSize,windowSlide)
            obj.order = order;
            obj.framelength = framelength;
            obj.iteration = iteration;
            obj.columnNo = columnNo;
            obj.vectorV = vectorV;
            obj.movingAverageLength = movingAverageLength;
            obj.stepSize = stepSize;
            obj.windowSlide = windowSlide;
        end
         
        %Savitzky-Golay filtering - allows polynomials
        function twoDSavitzky = SavitzkyGFilter(obj,twoDLayer) 
            twoDSavitzky(:,:) = sgolayfilt(twoDLayer,obj.order,obj.framelength);
        end
        
        %Moving Average Filter
        function twoDMovingAverage = movingAverage(obj,twoDLayer,movingAverageLength)
            for column = 1:obj.columnNo
            obj.twoDMovingAv(:,column) = conv(twoDLayer(:,column),obj.vectorV);
            end
            
            cutting = (movingAverageLength - 1)/2;
            lengthArrayCut = size(obj.twoDMovingAv,1) - cutting;
            disp(lengthArrayCut);
            twoDMovingAverage(:,:) = obj.twoDMovingAv(cutting+1:lengthArrayCut,:,:);

        end
        
      
        function [RotTrans,DataTr] = TimeDomainFE_Ver3(obj,isRegression,Data)
        %This code is for Feature Extraction (FE) for EMG based predictions. The version
        %3 aims for FE during a Pseudo online Predition through EMG activations.
        %           Author: Anany Dwivedi
        %           Date  : Mar-13-18
        %           The University of Auckland
        %   Detailed explanation goes here


        %         MAV = mean(abs(Data1(i:i+windowSize,ch)));
        %         logvar = log(var(Data1(i:i+windowSize,ch)));
        % Data1 = gpuArray(Data1);
            startPoint = 1;
            if isRegression
                startCh = 7;
            else
                startCh = 1;
            end
            totalCh = obj.columnNo;
            windowSize = obj.movingAverageLength;
            endPoint = length(Data) - mod(length(Data),1000);
            Data1=Data(startPoint+1:obj.stepSize:endPoint,:);
        %     Data1 = MeanProcessing(Data1);
            DataTr = [];
            RotTrans = [];
            Data1 = Data1.';
            for FtrExt = 0:obj.windowSlide:length(Data1)-(windowSize+1)
                DataSet = Data1(startCh:totalCh,FtrExt+1:FtrExt+windowSize+1);
                numFE = 1;
                timeDomainFeatures = (zeros(1,size(DataSet,1)*numFE));
                RMS = zeros(size(DataSet,1),1);
                WL = zeros(size(DataSet,1),1);
                BZC = zeros(size(DataSet,1),1);
                %%
                % RMS = sqrt(mean((Data1.^2)));
                RMS = sqrt(mean((DataSet.^2),2));
                %%
                % WL = sum(abs(diff(Data1)));
                WL = sum(abs(diff(DataSet,1,2)),2);
                %%
                % dif = diff(sign(Data1),1);
                % BZC = sum(dif ~= 0);
                dif = diff(sign(DataSet),1,2);
                BZC = sum(dif ~= 0,2);
                %%
                timeDomainFeatures(1:numFE:end) = RMS;
                %timeDomainFeatures(2:numFE:end) = WL;
                %timeDomainFeatures(3:numFE:end) = BZC;
                clear RMS WL BZC
                if isRegression
                    RotTrans = [RotTrans ; Data1(1:6,FtrExt+windowSize+1).'];
                else
                    RotTrans = double.empty((length(Data1)-(windowSize+1)),0);
                end
                DataTr = [DataTr;  timeDomainFeatures];
            end
        end
    end
end